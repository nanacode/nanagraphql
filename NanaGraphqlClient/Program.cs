﻿using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace NanaGraphqlClient
{
    class Program
    {
        private static bool _allowUntrustedCertificates = true;
        private static HttpClient _forceOwnHttpClient = null;

        private static HttpClient GetHttpClient()
        {
            if (_forceOwnHttpClient != null)
                return _forceOwnHttpClient;

            if (_allowUntrustedCertificates)
            {
                var handler = new HttpClientHandler();
                handler.ClientCertificateOptions = ClientCertificateOption.Manual;
                handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };

                return new HttpClient(handler);
            }
            else
                return new HttpClient();
        }

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            Console.WriteLine("Hello GraphQL. Press any key...");
            Console.ReadKey();

            var graphQLOptions = new GraphQLHttpClientOptions
            {
                EndPoint = new Uri("http://localhost:64567", UriKind.Absolute),
            };
            var graphQLClient = new GraphQLHttpClient(graphQLOptions, new NewtonsoftJsonSerializer(), GetHttpClient());

            var customersOfProductReq = new GraphQLRequest
            {
                Query = @"
                {
                  product(where: {OR: [{productId:1}, {productId:2}]} ){
                    categoryId
                    supplierId
                    productName
                    orderDetails{
                      order{
                        customer{
                          contactName
                        }
                      }
                    }
                  }
                }
                "
            };

            var graphQLResponse = await graphQLClient.SendQueryAsync<dynamic>(customersOfProductReq).ConfigureAwait(false);
            Console.WriteLine(graphQLResponse.Data);

            Console.WriteLine("Bye GraphQL");

            Console.ReadKey();
        }
    }
}

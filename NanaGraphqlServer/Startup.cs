using DatabaseAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using System.IO;
using HotChocolate.AspNetCore;
using HotChocolate.Types;
using HotChocolate;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate.Subscriptions;

namespace NanaApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private IWebHostEnvironment _webHostEnvironment;


        /// <summary>
        /// #DETECTS PROPER IP ADDRESSES
        /// </summary>
        public void ConfigureIpAddresses(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
        }

        /// <summary>
        /// #ALLOWS TO RETURN OWN 400 RESPONSE
        /// </summary>
        public void TurnOffAuto400(IServiceCollection services)
        {
            services.Configure<Microsoft.AspNetCore.Mvc.ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        /// <summary>
        /// #FORCE USING NEWTONSOFT DURING API CONVERSIONS
        /// </summary>
        public void ForceNewtonsoft(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
        }

        /// <summary>
        /// #INJECT DBCONTEXT
        /// </summary>
        public void InjectDbContext(IServiceCollection services)
        {
            var x = Configuration.GetConnectionString("nanaConnectionString");
            //#Connection string
            services.AddDbContext<hobbitsContext>(options => options.UseSqlite(Configuration.GetConnectionString("nanaConnectionString")));
        }

        /// <summary>
        /// #INJECT SERVICES
        /// </summary>
        public void InjectServices(IServiceCollection services)
        {
            //#Standard DI
            //services.AddScoped<IService, Class>();
            //services.AddTransient<IService>(x => new Class(
            //    Configuration.GetSection("Section")["field1"],
            //    Configuration.GetSection("Section")["field2"],
            //    Configuration.GetSection("Section")["field3"]
            //    ));


            //#Differ DI
            if (Configuration.GetValue<string>("EnvSettingsName") == "Development")
            {
                //services.AddScoped<IService, Class>();
            }
            else
            {
                //services.AddScoped<IService, Class>();
            }
        }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGraphQL(SchemaBuilder.New()
                .AddQueryType<Query>()
                .AddMutationType<Mutation>()
                );

            TurnOffAuto400(services);
            InjectServices(services);
            InjectDbContext(services);
            ForceNewtonsoft(services);
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            _webHostEnvironment = env;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            //cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            ConfigureIpAddresses(app, env);
            app.UseGraphQL();
        }
    
    }

    public class Query
    {
        [UseSelection] //[UsePaging]
        [UseFiltering]
        [UseSorting]
        public IQueryable<Customers> Customers([Service] hobbitsContext context) => context.Customers;
        [UseSelection]
        [UseFiltering]
        [UseSorting]
        public IQueryable<Products> Product([Service] hobbitsContext context) => context.Products;
     
    }

    public class Mutation
    {
        public async Task<Products> AddProduct(string name, string quantityPerUnit, string discontinued, [Service]hobbitsContext context)
        {
            var product = new Products
            {
                ProductName = name,
                QuantityPerUnit = quantityPerUnit,
                Discontinued = discontinued,
                CategoryId = 1,
                SupplierId = 1
            };
            context.Products.Add(product);
            await context.SaveChangesAsync();
            return product;
        }
    }
}

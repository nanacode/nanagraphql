﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using DatabaseAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;


namespace NanaApi.Controllers
{

    [Route("[controller]")]
    public class NanaController : Controller
    {
        private readonly ILogger<NanaController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IServiceProvider _services;

        public NanaController(ILogger<NanaController> logger, IConfiguration configuration, IServiceProvider services)
        {
            _logger = logger;
            _configuration = configuration;
            _services = services;
            //If you don't want to (or if you are not able to) injects via constructor:
            //var ticketStrategy = ActivatorUtilities.CreateInstance<SkekTicketStrategy>(_services);
        }

        [HttpGet("[action]")]
        public ActionResult Ping()
        {
            var dao = ActivatorUtilities.CreateInstance<ProductsDAO>(_services);

            var product = new Products
            {
                ProductName = "Chlebek",
                QuantityPerUnit = "kg",
                Discontinued = "nope",
                SupplierId = 1,
                CategoryId = 1
            };
            dao.Create(product);
            return Ok("I know you, you can go in.");
        }

        [HttpGet("[action]")]
        public ActionResult PingDb()
        {
            return Ok("I chlebek added.");
        }

    }
}

﻿# Features
## [framework base]
- Web Api + MVC on the one host

- MVC example with Models (WebModels), Controller and Views (with layout and partial)

- DI Examples (manually/via constructor)

## [logging]

- Serilog ("to file", "json compact format", as you like)

## [web functions]

-  Environment handling (kestrel settings, differing DI, endpoint with env knowledge)

-  Detecting proper incoming IP addresses and allowing custom 400

-  Easy ResourcesReader (for CopyAlways files)

-  Using Newtonsoft as official Controllers serializer/deserializer 

-  Reading raw data from request body (if you don't want to use model on the method input)

-  Static Files (Assets) hosting and routing

- [ACTION FILTERS] "Traces" (Activity Tags) with: action filter dumping tags to log, IP registering

- [AUTH FILTERS] ApiKey Authorization attribute

## [useful lib]

- Injecting dbcontext

- Test Smtp - dummy smtp connector, emails service (with attachments) and that wonderful smtp server jar file from the abyss of the Internet 

- [AES] Symmetric Encryption/Decryption with AES, 128bits-key generator with given salt (wink wink, a perfect key for AES)

- [DataAnnotations] Manuall validation using DataAnnotations (eg. [Required])

- Json serialization ignoring [Newtonsoft.Json.JsonIgnore] attribute

- Sqlite example
﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseAccess
{
    public class ProductsDAO
    {
        private readonly hobbitsContext _context;

        public ProductsDAO(hobbitsContext context)
        {
            _context = context;
        }

        public bool Create(Products prod)
        {
            _context.Products.Add(prod);
            var changedItems = _context.SaveChanges();

            if (changedItems > 0)
            {
                return true;
            }

            return false;
        }
    }
}

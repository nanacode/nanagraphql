﻿using System;
using System.Collections.Generic;

namespace DatabaseAccess
{
    public partial class Hobbits
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
